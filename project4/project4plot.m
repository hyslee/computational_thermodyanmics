clc; clear all;
nx=81;  ny=61; % why should I subtract 1 to each grid point?
load Tsteady_c25_nx81ny61.txt;
i=0; j=1;
for k=1:(nx)*(ny);
    i=i+1;
    if(i>nx);
        i=1;
        j=j+1;
    end
    x(i,j)=Tsteady_c25_nx81ny61(k,1);
    y(i,j)=Tsteady_c25_nx81ny61(k,2);
    tmp(i,j)=Tsteady_c25_nx81ny61(k,3);
end
subplot(2,2,1)
contour(x,y,tmp),xlabel('x'),ylabel('y')
subplot(2,2,2)
mesh(x,y,tmp),xlabel('x'),ylabel('y'),zlabel('T[K]')
subplot(2,2,3)
meshc(x,y,tmp),xlabel('x'),ylabel('y'),zlabel('T[K]')
subplot(2,2,4)
surf(x,y,tmp),xlabel('x'),ylabel('y'),zlabel('T[K]')