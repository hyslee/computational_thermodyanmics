CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C This program
C delta x = delta y
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      Program project2
      include 'project2params.f'

      call initialize
      call output
      do 3 i=2, nx-1
      do 4 j=2, ny-1
        temp(i,j)=1.0d+0
4     continue
3     continue

      n=0
10    n=n+1

      if(time.ge.tfinal) go to 20


      call integral
      time=time+deltat

      print*, 'iteration, time, error', n, time, error, temp((nx/2)-1,
     & (ny/2)-1)

      open(unit=60,file='error_c25_nx252ny190.dat',status='unknown')
      write(60,202) time, error                       ! relative error
202   format(3(2x,d11.5))
      open(unit=70,file='Tcenter_c25_nx252ny190.txt',status='unknown')
      write(70,302) time, temp((nx/2)-1,(ny/2)-1)    ! center temperature
302   format(3(2x,d11.5))


      if(dabs(time-dnint(time)).le.0.5d+00*deltat) call output
      if(tfinal-time.lt.deltat) deltat=tfinal-time
      go to 10

20    continue


      stop
      end program project2
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine initialize
      include 'wowproject2.f'

      time=0.d+0
      Lx_non=1.d+0
      Ly_non=0.75d+0
      courant = 0.25d+0       ! delta x = delta y, so only one courant number
      deltax = Lx_non /(NX-1)
      deltay = deltax
      deltat = courant * (deltax**(2.d+0))
      tfinal= 1d+0         ! time end


      do 10 i=1, nx-1
        do 20 j=1, ny-1
        x(i)=dble(float(i-1))*deltax  ! post processing x axis
        y(j)=dble(float(j-1))*deltay  ! post processing y axis
20      continue
10    continue

      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine integral
      include 'wowproject2.f'

      error=0.d+00

      do 10 j=1, ny       ! boundary condition
       do 20 i=1, nx
         temp(1,j)=1
         temp(i,1)=1
         temp(nx,j)=1
         temp(i,ny)=3
20     continue
10    continue

       do 30 j=2, ny-1   ! Central space integral method
        do 40 i=2, nx-1
         temp0(i,j)=temp(i,j)+courant*(temp(i+1,j)-(2*temp(i,j))
     &   +temp(i-1,j))+courant*(temp(i,j+1)-(2*temp(i,j))
     &   +temp(i,j-1))

         error=error+dabs(temp0(i,j)-temp(i,j))/dble(float((NY*NX)))   ! error calculation
40      continue
30     continue

      Do 50 j=1, ny
       do 60 i=1, nx
        temp(i,j)=temp0(i,j)
60     continue
50    continue


      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine output
      include 'wowproject2.f'

      open(unit=50,file='Tsteady_c25_nx252ny190.dat',status='unknown')
      do 59 j=1,NY
        do 58 i=1,NX
         write(50,102) x(i),y(j),temp(i,j) ! steady state temperature
58      continue
59    continue
      close(unit=50)
102   format(3(2x,d11.5))


      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
