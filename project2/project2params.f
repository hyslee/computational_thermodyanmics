
      parameter(nx=252) ! (nx-1)/(ny-1) = 1/0.75
      parameter(ny=190) ! ny= 1+(0.75/1)*(nx-1)
      integer n

      real*8 courant, deltat, deltax, deltay, Lx_non, Ly_non, time
      real*8 tfinal, error, temp0(nx,ny), temp(nx,ny)
      real*8 x(nx), y(ny)
      
      common /data1/ courant, deltat, deltax, deltay, Lx_non,
     &   Ly_non, time, tfinal, error, temp0, temp, x, y, n
